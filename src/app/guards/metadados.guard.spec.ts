import { TestBed } from '@angular/core/testing';

import { MetadadosGuard } from './metadados.guard';

describe('MetadadosGuard', () => {
  let guard: MetadadosGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(MetadadosGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
