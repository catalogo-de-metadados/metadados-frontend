import { Component, OnInit } from '@angular/core';
import { RelatorioService } from 'src/app/services/relatorio.service';
import { Banco } from '../models/banco';
import { BancoService } from '../services/banco.service';


@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit {

  banco: Banco;
  bancos: Banco[];

  constructor(
    private relatorioService: RelatorioService,
    private bancoService: BancoService
  ) { }

  ngOnInit(): void {
    this.loadBancos();
  }

  onImprimir() {
    this.relatorioService.generate(this.banco);
  }

  loadBancos(): void {
    this.bancoService.list().subscribe(
      data => this.bancos = data['content'],
      err => console.log(err)
    );
  }


}
