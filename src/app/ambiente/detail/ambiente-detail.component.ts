import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AmbienteService } from 'src/app/services/ambiente.service';
import { Ambiente } from 'src/app/models/ambiente';

@Component({
  selector: 'app-ambiente-detail',
  templateUrl: './ambiente-detail.component.html',
  styleUrls: ['./ambiente-detail.component.css']
})
export class AmbienteDetailComponent implements OnInit {

  ambiente: Ambiente;

  constructor(
    private ambienteService: AmbienteService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.ambienteService.detail(id).subscribe(
      data => {
        this.ambiente = data;
      },
      err => console.log(err)
    );
  }

  voltar(): void {
    this.router.navigate(['/ambiente/listar']);
  }

}