import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ExportService } from 'src/app/services/export.service';
import { LoginService } from 'src/app/services/login.service';
import { AmbienteService } from 'src/app/services/ambiente.service';
import { Ambiente } from 'src/app/models/ambiente';

@Component({
  selector: 'app-ambiente-list',
  templateUrl: './ambiente-list.component.html',
  styleUrls: ['./ambiente-list.component.css']
})
export class AmbienteListComponent implements OnInit {

  ambientes: Ambiente[] = [];
  temp: Ambiente[];

  isAdmin: boolean;

  constructor(
    private ambienteService: AmbienteService,
    private loginService: LoginService,
    private exportService: ExportService,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.loadAmbientes();
    this.isAdmin = this.loginService.getIsAdmin();
  }

  loadAmbientes(): void {
    this.ambienteService.list().subscribe(
      data => {
        this.ambientes = data['content'];
        this.temp = this.ambientes;
      },
      err => console.log(err)
    );
  }

  onDelete(id: number): void {
    this.ambienteService.delete(id).subscribe(
      data => {
        this.toastrService.success(data.message);
        this.loadAmbientes();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  exportexcel(): void {
    this.exportService.export(this.ambientes, "ambiente");
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.nmAmbiente.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.ambientes = temp;
  }

}