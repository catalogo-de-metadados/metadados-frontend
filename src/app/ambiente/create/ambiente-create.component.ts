import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { AmbienteService } from 'src/app/services/ambiente.service';
import { Ambiente } from 'src/app/models/ambiente';

@Component({
  selector: 'app-ambiente-create',
  templateUrl: './ambiente-create.component.html',
  styleUrls: ['./ambiente-create.component.css']
})
export class AmbienteCreateComponent implements OnInit {

  ambiente: Ambiente;

  nmAmbiente: string;

  constructor(
    private ambienteService: AmbienteService,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
  }

  onCreate(): void {
    this.ambiente = new Ambiente(this.nmAmbiente);
    this.ambienteService.create(this.ambiente).subscribe(
      data => {
        this.toastrService.success("Ambiente incluído com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['/ambiente']);
  }
}

