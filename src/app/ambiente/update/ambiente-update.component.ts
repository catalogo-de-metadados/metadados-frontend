import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { AmbienteService } from 'src/app/services/ambiente.service';
import { Ambiente } from 'src/app/models/ambiente';

@Component({
  selector: 'app-ambiente-update',
  templateUrl: './ambiente-update.component.html',
  styleUrls: ['./ambiente-update.component.css']
})
export class AmbienteUpdateComponent implements OnInit {

  ambiente: Ambiente;

  constructor(
    private ambienteService: AmbienteService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.ambienteService.detail(id).subscribe(
      data => {
        this.ambiente = data;
      },
      err => console.log(err)
    );
  }

  onUpdate(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.ambiente.id = undefined;
    this.ambiente.dtRegistro = undefined;
    this.ambienteService.update(id, this.ambiente).subscribe(
      data => {
        this.toastrService.success("Ambiente alterado com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['/ambiente/listar']);
  }

}


