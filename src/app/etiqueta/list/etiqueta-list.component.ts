import { Component, OnInit } from '@angular/core';
import { Etiqueta } from 'src/app/models/etiqueta';
import { ExportService } from 'src/app/services/export.service';
import { LoginService } from 'src/app/services/login.service';
import { EtiquetaService } from 'src/app/services/etiqueta.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-etiqueta-list',
  templateUrl: './etiqueta-list.component.html',
  styleUrls: ['./etiqueta-list.component.css']
})
export class EtiquetaListComponent implements OnInit {

  etiquetas: Etiqueta[] = [];
  temp: Etiqueta[];

  isAdmin: boolean;

  constructor(
    private etiquetaService: EtiquetaService,
    private loginService: LoginService,
    private exportService: ExportService,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.loadEtiquetas();
    this.isAdmin = this.loginService.getIsAdmin();
  }

  loadEtiquetas(): void {
    this.etiquetaService.list().subscribe(
      data => {
        this.etiquetas = data['content'];
        this.temp = this.etiquetas;
      },
      err => console.log(err)
    );
  }

  onDelete(id: number): void {
    this.etiquetaService.delete(id).subscribe(
      data => {
        this.toastrService.success(data.message);
        this.loadEtiquetas();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  exportexcel(): void {
    this.exportService.export(this.etiquetas, "etiqueta");
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.nmEtiqueta.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.etiquetas = temp;
  }

}