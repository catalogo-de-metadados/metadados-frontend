import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { EtiquetaService } from './../../services/etiqueta.service';
import { Etiqueta } from 'src/app/models/etiqueta';


@Component({
  selector: 'app-etiqueta-update',
  templateUrl: './etiqueta-update.component.html',
  styleUrls: ['./etiqueta-update.component.css']
})
export class EtiquetaUpdateComponent implements OnInit {

  etiqueta: Etiqueta;

  constructor(
    private etiquetaService: EtiquetaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.etiquetaService.detail(id).subscribe(
      data => {
        this.etiqueta = data;
      },
      err => console.log(err)
    );
  }

  onUpdate(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.etiqueta.id = undefined;
    this.etiqueta.dtRegistro = undefined;
    this.etiquetaService.update(id, this.etiqueta).subscribe(
      data => {
        this.toastrService.success("Etiqueta alterada com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['/etiqueta/listar']);
  }

}

