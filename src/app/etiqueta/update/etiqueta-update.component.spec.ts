import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtiquetaUpdateComponent } from './etiqueta-update.component';

describe('EtiquetaUpdateComponent', () => {
  let component: EtiquetaUpdateComponent;
  let fixture: ComponentFixture<EtiquetaUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtiquetaUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtiquetaUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
