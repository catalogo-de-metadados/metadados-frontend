import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { Etiqueta } from 'src/app/models/etiqueta';
import { EtiquetaService } from './../../services/etiqueta.service';

@Component({
  selector: 'app-etiqueta-create',
  templateUrl: './etiqueta-create.component.html',
  styleUrls: ['./etiqueta-create.component.css']
})
export class EtiquetaCreateComponent implements OnInit {

  etiqueta: Etiqueta;

  nmEtiqueta: string;
  inTipoEtiqueta: string;


  constructor(
    private etiquetaService: EtiquetaService,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
  }


  onCreate(): void {
    this.etiqueta = new Etiqueta(this.nmEtiqueta, this.inTipoEtiqueta);
    this.etiquetaService.create(this.etiqueta).subscribe(
      data => {
        this.toastrService.success("Etiqueta incluída com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['/etiqueta']);
  }
}

