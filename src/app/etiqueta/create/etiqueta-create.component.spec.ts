import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtiquetaCreateComponent } from './etiqueta-create.component';

describe('EtiquetaCreateComponent', () => {
  let component: EtiquetaCreateComponent;
  let fixture: ComponentFixture<EtiquetaCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtiquetaCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtiquetaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
