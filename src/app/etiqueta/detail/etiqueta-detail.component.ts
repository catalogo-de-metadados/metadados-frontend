import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Etiqueta } from 'src/app/models/etiqueta';
import { EtiquetaService } from './../../services/etiqueta.service';

@Component({
  selector: 'app-etiqueta-detail',
  templateUrl: './etiqueta-detail.component.html',
  styleUrls: ['./etiqueta-detail.component.css']
})
export class EtiquetaDetailComponent implements OnInit {

  etiqueta: Etiqueta;

  constructor(
    private etiquetaService: EtiquetaService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.etiquetaService.detail(id).subscribe(
      data => {
        this.etiqueta = data;
      },
      err => console.log(err)
    );
  }

  voltar(): void {
    this.router.navigate(['/etiqueta/listar']);
  }

}