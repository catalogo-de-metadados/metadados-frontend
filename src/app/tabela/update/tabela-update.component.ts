import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TabelaService } from './../../services/tabela.service';
import { Tabela } from './../../models/tabela';

@Component({
  selector: 'app-tabela-update',
  templateUrl: './tabela-update.component.html',
  styleUrls: ['./tabela-update.component.css']
})
export class TabelaUpdateComponent implements OnInit {

  tabela: Tabela;
  params: any;

  constructor(
    private tabelaService: TabelaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
    });
    this.tabelaService.detail(this.params.idTabela).subscribe(
      data => this.tabela = data,
      err => console.log(err)
    );
  }

  onUpdate(): void {
    this.tabela.id = undefined;
    this.tabela.dtRegistro = undefined;
    this.tabelaService.update(this.params.idTabela, this.tabela).subscribe(
      data => {
        this.toastrService.success("Tabela alterada com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema', this.params.idEsquema, 'tabela']);
  }

}
