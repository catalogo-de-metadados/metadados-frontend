import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaDetailComponent } from './tabela-detail.component';

describe('TabelaDetailComponent', () => {
  let component: TabelaDetailComponent;
  let fixture: ComponentFixture<TabelaDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabelaDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
