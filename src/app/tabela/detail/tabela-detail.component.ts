import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tabela } from './../../models/tabela';
import { TabelaService } from './../../services/tabela.service';

@Component({
  selector: 'app-tabela-detail',
  templateUrl: './tabela-detail.component.html',
  styleUrls: ['./tabela-detail.component.css']
})
export class TabelaDetailComponent implements OnInit {

  tabela: Tabela;
  params: any;

  constructor(
    private tabelaService: TabelaService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
    });
    this.tabelaService.detail(this.params.idTabela).subscribe(
      data => this.tabela = data,
      err => console.log(err)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema', this.params.idEsquema, 'tabela']);
  }

}