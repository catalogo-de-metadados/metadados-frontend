import { Component, OnInit } from '@angular/core';
import { LoginService } from './../../services/login.service';
import { ExportService } from './../../services/export.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { TabelaService } from './../../services/tabela.service';
import { Tabela } from './../../models/tabela';
import { EsquemaService } from 'src/app/services/esquema.service';
import { Esquema } from 'src/app/models/esquema';

@Component({
  selector: 'app-tabela-list',
  templateUrl: './tabela-list.component.html',
  styleUrls: ['./tabela-list.component.css']
})
export class TabelaListComponent implements OnInit {

  tabelas: Tabela[] = [];
  esquema: Esquema;
  temp: Tabela[];

  isAdmin: boolean;

  params: any;

  constructor(
    private tabelaService: TabelaService,
    private loginService: LoginService,
    private exportService: ExportService,
    private toastrService: ToastrService,
    private esquemaService: EsquemaService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
      this.loadTabelas(params.idEsquema);
      this.loadEsquema(params.idEsquema);
    });
    this.isAdmin = this.loginService.getIsAdmin();
  }

  loadTabelas(id): void {
    this.tabelaService.listAllByEsquemaId(id).subscribe(
      data => {
        this.tabelas = data['content'];
        this.temp = this.tabelas;
      },
      err => console.log(err)
    );
  }

  loadEsquema(id): void {
    this.esquemaService.detail(id).subscribe(
      data => {
        this.esquema = data;
      },
      err => console.log(err)
    );
  }

  onDelete(id: number): void {
    this.tabelaService.delete(id).subscribe(
      data => {
        this.toastrService.success(data.message);
        this.loadTabelas(this.params.idEsquema);
      },
      err =>
        this.toastrService.error(err.error.message)
    );
  }

  exportexcel(): void {
    this.exportService.export(this.tabelas, "tabelas");
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.nmTabela.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.tabelas = temp;
  }

}
