import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Esquema } from 'src/app/models/esquema';
import { EsquemaService } from 'src/app/services/esquema.service';
import { Tabela } from './../../models/tabela';
import { TabelaService } from './../../services/tabela.service';

@Component({
  selector: 'app-tabela-create',
  templateUrl: './tabela-create.component.html',
  styleUrls: ['./tabela-create.component.css']
})
export class TabelaCreateComponent implements OnInit {

  tabela: Tabela;
  params: any;

  nmTabela: string;
  txDescricao: string;
  esquema: Esquema;

  constructor(
    private tabelaService: TabelaService,
    private esquemaService: EsquemaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
      this.loadEsquema(params.idEsquema);
    });
  }

  onCreate(): void {
    this.tabela = new Tabela(this.nmTabela, this.txDescricao, this.esquema);
    this.tabelaService.create(this.tabela).subscribe(
      data => {
        this.toastrService.success("Tabela incluída com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  loadEsquema(id): void {
    this.esquemaService.detail(id).subscribe(
      data => this.esquema = data,
      err => console.log(err)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema', this.params.idEsquema, 'tabela']);
  }
}
