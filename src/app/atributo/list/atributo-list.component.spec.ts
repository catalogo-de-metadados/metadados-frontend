import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtributoListComponent } from './atributo-list.component';

describe('AtributoListComponent', () => {
  let component: AtributoListComponent;
  let fixture: ComponentFixture<AtributoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtributoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtributoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
