import { Component, OnInit } from '@angular/core';
import { LoginService } from './../../services/login.service';
import { ExportService } from './../../services/export.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { AtributoService } from './../../services/atributo.service';
import { Atributo } from './../../models/atributo';
import { TabelaService } from 'src/app/services/tabela.service';
import { Tabela } from 'src/app/models/tabela';

@Component({
  selector: 'app-atributo-list',
  templateUrl: './atributo-list.component.html',
  styleUrls: ['./atributo-list.component.css']
})
export class AtributoListComponent implements OnInit {

  atributos: Atributo[] = [];
  tabela: Tabela;
  temp: Atributo[];

  isAdmin: boolean;

  params: any;

  constructor(
    private atributoService: AtributoService,
    private tabelaService: TabelaService,
    private loginService: LoginService,
    private exportService: ExportService,
    private toastrService: ToastrService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
      this.loadAtributos(params.idTabela);
      this.loadTabela(params.idTabela);
    });
    this.isAdmin = this.loginService.getIsAdmin();
  }

  loadAtributos(id): void {
    this.atributoService.listAllByTabelaId(id).subscribe(
      data => {
        this.atributos = data['content'];
        this.temp = this.atributos;
      },
      err => console.log(err)
    );
  }

  loadTabela(id): void {
    this.tabelaService.detail(id).subscribe(
      data => {
        this.tabela = data;
      },
      err => console.log(err)
    );
  }

  onDelete(id: number): void {
    this.atributoService.delete(id).subscribe(
      data => {
        this.toastrService.success(data.message);
        this.loadAtributos(this.params.idTabela);
      },
      err =>
        this.toastrService.error(err.error.message)
    );
  }

  exportexcel(): void {
    this.exportService.export(this.atributos, "atributos");
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.nmAtributo.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.atributos = temp;
  }

}

