import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Etiqueta } from 'src/app/models/etiqueta';
import { EtiquetaService } from 'src/app/services/etiqueta.service';
import { Atributo } from './../../models/atributo';
import { AtributoService } from './../../services/atributo.service';

@Component({
  selector: 'app-lgpd',
  templateUrl: './lgpd.component.html',
  styleUrls: ['./lgpd.component.css']
})
export class LgpdComponent implements OnInit {

  atributo: Atributo;
  etiquetas: Etiqueta[];
  temp: Etiqueta[];
  etiquetasSelecionadas = [];

  params: any;

  constructor(
    private atributoService: AtributoService,
    private etiquetaService: EtiquetaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
    });
    this.atributoService.detail(this.params.idAtributo).subscribe(
      data => {
        this.atributo = data;
        this.loadEtiquetas();
        this.etiquetasSelecionadas = this.atributo.etiqueta
      },
      err => console.log(err)
    );
  }

  loadEtiquetas() {
    this.etiquetaService.list().subscribe(
      data => {
        this.temp = data['content'];
        this.etiquetas = this.filter(this.temp);
      },
      err => console.log(err)
    );
  }

  filter(etiquetas: Etiqueta[]): Etiqueta[] {
    let temp: Etiqueta[] = [];
    if (this.atributo.inSensivel == "S")
      temp = [...temp, ...etiquetas.filter((obj) => obj.inTipoEtiqueta == "S")];
    if (this.atributo.inPessoal == "S")
      temp = [...temp, ...etiquetas.filter((obj) => obj.inTipoEtiqueta == "P")];
    return temp;
  }

  onChange() {
    this.etiquetasSelecionadas = [];
    this.etiquetas = this.filter(this.temp);
  }

  onUpdate(): void {
    this.atributo.id = undefined;
    this.atributo.dtRegistro = undefined;
    if (this.atributo.inSensivel == "") this.atributo.inSensivel = undefined;
    if (this.atributo.inPessoal == "") this.atributo.inPessoal = undefined;
    this.atributo.etiqueta = this.etiquetasSelecionadas;
    this.atributoService.update(this.params.idAtributo, this.atributo).subscribe(
      data => {
        this.toastrService.success("Classificação do atributo realizada com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema', this.params.idEsquema, 'tabela', this.params.idTabela, 'atributo']);
  }

}

