import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Contribuicao } from 'src/app/models/contribuicao';
import { ContribuicaoService } from 'src/app/services/contribuicao.service';
import { LoginService } from 'src/app/services/login.service';
import { Atributo } from './../../models/atributo';
import { AtributoService } from './../../services/atributo.service';

@Component({
  selector: 'app-contribuicao',
  templateUrl: './contribuicao.component.html',
  styleUrls: ['./contribuicao.component.css']
})
export class ContribuicaoComponent implements OnInit {

  isAdmin: boolean;
  userName: string;

  contribuicao: Contribuicao;
  contribuicoes: Contribuicao[];

  params: any;

  txContribuicao: string;
  atributo: Atributo;

  constructor(
    private atributoService: AtributoService,
    private contribuicaoService: ContribuicaoService,
    private loginService: LoginService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.isAdmin = this.loginService.getIsAdmin();
    this.userName = this.loginService.getUsername();
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
      this.loadAtributo(params.idAtributo)
      this.loadContribuicoes(params.idAtributo);
    });
  }

  loadAtributo(id): void {
    this.atributoService.detail(id).subscribe(
      data => this.atributo = data,
      err => console.log(err)
    );
  }

  loadContribuicoes(id): void {
    this.contribuicaoService.listAllContribuicoesByAtributoId(id).subscribe(
      data => this.contribuicoes = data,
      err => console.log(err)
    );
  }

  onCreate(): void {

    let contribuicao = new Contribuicao(
      this.txContribuicao,
      this.userName,
      this.atributo
    );
    this.contribuicaoService.create(contribuicao).subscribe(
      data => {
        this.toastrService.success("Contribuição incluída com sucesso.");
        this.loadContribuicoes(this.params.idAtributo);
        this.txContribuicao = null;
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  onDelete(id: number): void {
    this.contribuicaoService.delete(id).subscribe(
      data => {
        this.toastrService.success(data.message);
        this.loadContribuicoes(this.params.idAtributo);
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema', this.params.idEsquema, 'tabela', this.params.idTabela, 'atributo']);
  }

}