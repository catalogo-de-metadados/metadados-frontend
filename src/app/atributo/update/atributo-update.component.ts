import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Atributo } from './../../models/atributo';
import { AtributoService } from './../../services/atributo.service';

@Component({
  selector: 'app-atributo-update',
  templateUrl: './atributo-update.component.html',
  styleUrls: ['./atributo-update.component.css']
})
export class AtributoUpdateComponent implements OnInit {

  atributo: Atributo;
  params: any;

  constructor(
    private atributoService: AtributoService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
    });
    this.atributoService.detail(this.params.idAtributo).subscribe(
      data => this.atributo = data,
      err => console.log(err)
    );
  }

  onUpdate(): void {
    this.atributo.id = undefined;
    this.atributo.dtRegistro = undefined;
    if (this.atributo.inSensivel == "") this.atributo.inSensivel = undefined;
    this.atributoService.update(this.params.idAtributo, this.atributo).subscribe(
      data => {
        this.toastrService.success("Atributo alterado com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema', this.params.idEsquema, 'tabela', this.params.idTabela, 'atributo']);
  }

}
