import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtributoUpdateComponent } from './atributo-update.component';

describe('AtributoUpdateComponent', () => {
  let component: AtributoUpdateComponent;
  let fixture: ComponentFixture<AtributoUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtributoUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtributoUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
