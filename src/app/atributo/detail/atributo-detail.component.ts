import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Atributo } from './../../models/atributo';
import { AtributoService } from './../../services/atributo.service';

@Component({
  selector: 'app-atributo-detail',
  templateUrl: './atributo-detail.component.html',
  styleUrls: ['./atributo-detail.component.css']
})
export class AtributoDetailComponent implements OnInit {

  atributo: Atributo;
  params: any;

  constructor(
    private atributoService: AtributoService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
    });
    this.atributoService.detail(this.params.idAtributo).subscribe(
      data => this.atributo = data,
      err => console.log(err)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema', this.params.idEsquema, 'tabela', this.params.idTabela, 'atributo']);
  }

}
