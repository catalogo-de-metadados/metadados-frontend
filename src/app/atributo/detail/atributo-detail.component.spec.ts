import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtributoDetailComponent } from './atributo-detail.component';

describe('AtributoDetailComponent', () => {
  let component: AtributoDetailComponent;
  let fixture: ComponentFixture<AtributoDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtributoDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtributoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
