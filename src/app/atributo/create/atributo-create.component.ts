import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Tabela } from 'src/app/models/tabela';
import { TabelaService } from 'src/app/services/tabela.service';
import { Atributo } from './../../models/atributo';
import { AtributoService } from './../../services/atributo.service';

@Component({
  selector: 'app-atributo-create',
  templateUrl: './atributo-create.component.html',
  styleUrls: ['./atributo-create.component.css']
})
export class AtributoCreateComponent implements OnInit {

  atributo: Atributo;
  params: any;

  nmAtributo: string;
  tipo: string;
  tamanho: number;
  inCampoChave: boolean;
  inCampoFk: boolean;
  inObrigatorio: boolean;
  txDescricao: string;
  tabela: Tabela;

  constructor(
    private atributoService: AtributoService,
    private tabelaService: TabelaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
      this.loadTabela(params.idTabela)
    });
  }

  loadTabela(id): void {
    this.tabelaService.detail(id).subscribe(
      data => this.tabela = data,
      err => console.log(err)
    );
  }

  onCreate(): void {
    this.atributo = new Atributo(
      this.nmAtributo,
      undefined,
      undefined,
      this.tipo,
      this.tamanho,
      this.inCampoChave,
      this.inCampoFk,
      this.inObrigatorio,
      this.txDescricao,
      undefined,
      this.tabela
    );
    this.atributoService.create(this.atributo).subscribe(
      data => {
        this.toastrService.success("Atributo incluído com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema', this.params.idEsquema, 'tabela', this.params.idTabela, 'atributo']);
  }
}
