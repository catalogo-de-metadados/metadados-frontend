import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtributoCreateComponent } from './atributo-create.component';

describe('AtributoCreateComponent', () => {
  let component: AtributoCreateComponent;
  let fixture: ComponentFixture<AtributoCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtributoCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtributoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
