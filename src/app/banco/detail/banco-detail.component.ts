import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Banco } from 'src/app/models/banco';
import { BancoService } from 'src/app/services/banco.service';

@Component({
  selector: 'app-banco-detail',
  templateUrl: './banco-detail.component.html',
  styleUrls: ['./banco-detail.component.css']
})
export class BancoDetailComponent implements OnInit {

  banco: Banco;

  constructor(
    private bancoService: BancoService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.idBanco;
    this.bancoService.detail(id).subscribe(
      data => {
        this.banco = data;
      },
      err => console.log(err)
    );
  }

  voltar(): void {
    this.router.navigate(['/banco/listar']);
  }

}