import { Banco } from './../../models/banco';
import { ActivatedRoute, Router } from '@angular/router';
import { BancoService } from './../../services/banco.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AmbienteService } from 'src/app/services/ambiente.service';
import { Ambiente } from 'src/app/models/ambiente';
import { Tecnologia } from 'src/app/models/tecnologia';
import { TecnologiaService } from 'src/app/services/tecnologia.service';


@Component({
  selector: 'app-banco-update',
  templateUrl: './banco-update.component.html',
  styleUrls: ['./banco-update.component.css']
})
export class BancoUpdateComponent implements OnInit {

  banco: Banco;

  ambientes: Ambiente[];
  tecnologias: Tecnologia[];

  constructor(
    private bancoService: BancoService,
    private ambienteService: AmbienteService,
    private tecnologiaService: TecnologiaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.loadAmbientes();
    this.loadTecnologias();
    const id = this.activatedRoute.snapshot.params.idBanco;
    this.bancoService.detail(id).subscribe(
      data => {
        this.banco = data;
      },
      err => console.log(err)
    );
  }

  onUpdate(): void {
    const id = this.activatedRoute.snapshot.params.idBanco;
    this.banco.id = undefined;
    this.banco.dtRegistro = undefined;
    this.bancoService.update(id, this.banco).subscribe(
      data => {
        this.toastrService.success("Banco alterado com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  loadAmbientes() {
    this.ambienteService.list().subscribe(
      data => {
        this.ambientes = data['content'];
      },
      err => console.log(err)
    );
  }

  loadTecnologias() {
    this.tecnologiaService.list().subscribe(
      data => {
        this.tecnologias = data['content'];
      },
      err => console.log(err)
    );
  }

  voltar(): void {
    this.router.navigate(['/banco/listar']);
  }

}
