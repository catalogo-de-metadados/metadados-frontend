import { Component, OnInit } from '@angular/core';
import { BancoService } from './../../services/banco.service';
import { LoginService } from './../../services/login.service';
import { ExportService } from './../../services/export.service';
import { Banco } from './../../models/banco';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-banco-list',
  templateUrl: './banco-list.component.html',
  styleUrls: ['./banco-list.component.css']
})
export class BancoListComponent implements OnInit {

  bancos: Banco[] = [];
  temp: Banco[];


  isAdmin: boolean;

  constructor(
    private bancoService: BancoService,
    private loginService: LoginService,
    private exportService: ExportService,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.loadBancos();
    this.isAdmin = this.loginService.getIsAdmin();
  }

  loadBancos(): void {
    this.bancoService.list().subscribe(
      data => {
        this.bancos = data['content']; 
        this.temp = this.bancos;
      },
      err => console.log(err)
    );
  }

  onDelete(id: number): void {
    this.bancoService.delete(id).subscribe(
      data => {
        this.toastrService.success(data.message);
        this.loadBancos();
      },
      err =>
        this.toastrService.error(err.error.message)
    );
  }

  exportexcel(): void {
    this.exportService.export(this.bancos, "bancos");
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.nmBanco.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.bancos = temp;
  }

}
