import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Ambiente } from 'src/app/models/ambiente';
import { Tecnologia } from 'src/app/models/tecnologia';
import { AmbienteService } from 'src/app/services/ambiente.service';
import { TecnologiaService } from 'src/app/services/tecnologia.service';
import { Banco } from './../../models/banco';
import { BancoService } from './../../services/banco.service';

@Component({
  selector: 'app-banco-create',
  templateUrl: './banco-create.component.html',
  styleUrls: ['./banco-create.component.css']
})
export class BancoCreateComponent implements OnInit {

  banco: Banco;

  nmBanco: string;
  ambiente: Ambiente;
  tecnologia: Tecnologia;
  txDescricao: string;

  ambientes: Ambiente[];
  tecnologias: Tecnologia[];

  constructor(
    private bancoService: BancoService,
    private ambienteService: AmbienteService,
    private tecnologiaService: TecnologiaService,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.loadAmbientes();
    this.loadTecnologias();
  }

  onCreate(): void {
    this.banco = new Banco(this.nmBanco, this.ambiente, this.tecnologia, this.txDescricao);
    this.bancoService.create(this.banco).subscribe(
      data => {
        this.toastrService.success("Banco incluído com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  loadAmbientes() {
    this.ambienteService.list().subscribe(
      data => {
        this.ambientes = data['content'];
      },
      err => console.log(err)
    );
  }

  loadTecnologias() {
    this.tecnologiaService.list().subscribe(
      data => {
        this.tecnologias = data['content'];
      },
      err => console.log(err)
    );
  }

  voltar(): void {
    this.router.navigate(['/banco']);
  }

}
