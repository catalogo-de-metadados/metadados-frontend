import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ExportService } from 'src/app/services/export.service';
import { LoginService } from 'src/app/services/login.service';

import { TecnologiaService } from 'src/app/services/tecnologia.service';
import { Tecnologia } from 'src/app/models/tecnologia';

@Component({
  selector: 'app-tecnologia-list',
  templateUrl: './tecnologia-list.component.html',
  styleUrls: ['./tecnologia-list.component.css']
})
export class TecnologiaListComponent implements OnInit {

  tecnologias: Tecnologia[] = [];
  temp: Tecnologia[];

  isAdmin: boolean;

  constructor(
    private tecnologiaService: TecnologiaService,
    private loginService: LoginService,
    private exportService: ExportService,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.loadTecnologias();
    this.isAdmin = this.loginService.getIsAdmin();
  }

  loadTecnologias(): void {
    this.tecnologiaService.list().subscribe(
      data => {
        this.tecnologias = data['content'];
        this.temp = this.tecnologias;
      },
      err => console.log(err)
    );
  }

  onDelete(id: number): void {
    this.tecnologiaService.delete(id).subscribe(
      data => {
        this.toastrService.success(data.message);
        this.loadTecnologias();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  exportexcel(): void {
    this.exportService.export(this.tecnologias, "tecnologia");
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.nmTecnologia.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.tecnologias = temp;
  }

}