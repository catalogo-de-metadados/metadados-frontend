import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TecnologiaListComponent } from './tecnologia-list.component';

describe('TecnologiaListComponent', () => {
  let component: TecnologiaListComponent;
  let fixture: ComponentFixture<TecnologiaListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TecnologiaListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TecnologiaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
