import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TecnologiaCreateComponent } from './tecnologia-create.component';

describe('TecnologiaCreateComponent', () => {
  let component: TecnologiaCreateComponent;
  let fixture: ComponentFixture<TecnologiaCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TecnologiaCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TecnologiaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
