import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { TecnologiaService } from 'src/app/services/tecnologia.service';
import { Tecnologia } from 'src/app/models/tecnologia';

@Component({
  selector: 'app-tecnologia-create',
  templateUrl: './tecnologia-create.component.html',
  styleUrls: ['./tecnologia-create.component.css']
})
export class TecnologiaCreateComponent implements OnInit {

  tecnologia: Tecnologia;

  nmTecnologia: string;

  constructor(
    private tecnologiaService: TecnologiaService,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
  }

  onCreate(): void {
    this.tecnologia = new Tecnologia(this.nmTecnologia);
    this.tecnologiaService.create(this.tecnologia).subscribe(
      data => {
        this.toastrService.success("Tecnologia incluída com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['/tecnologia']);
  }
}

