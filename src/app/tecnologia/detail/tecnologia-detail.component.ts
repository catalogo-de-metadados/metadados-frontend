import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { TecnologiaService } from 'src/app/services/tecnologia.service';
import { Tecnologia } from 'src/app/models/tecnologia';

@Component({
  selector: 'app-tecnologia-detail',
  templateUrl: './tecnologia-detail.component.html',
  styleUrls: ['./tecnologia-detail.component.css']
})
export class TecnologiaDetailComponent implements OnInit {

  tecnologia: Tecnologia;

  constructor(
    private tecnologiaService: TecnologiaService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.tecnologiaService.detail(id).subscribe(
      data => {
        this.tecnologia = data;
      },
      err => console.log(err)
    );
  }

  voltar(): void {
    this.router.navigate(['/tecnologia/listar']);
  }

}