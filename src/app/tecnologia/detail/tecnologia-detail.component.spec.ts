import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TecnologiaDetailComponent } from './tecnologia-detail.component';

describe('TecnologiaDetailComponent', () => {
  let component: TecnologiaDetailComponent;
  let fixture: ComponentFixture<TecnologiaDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TecnologiaDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TecnologiaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
