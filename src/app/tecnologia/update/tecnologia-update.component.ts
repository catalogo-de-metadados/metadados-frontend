import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { TecnologiaService } from 'src/app/services/tecnologia.service';
import { Tecnologia } from 'src/app/models/tecnologia';

@Component({
  selector: 'app-tecnologia-update',
  templateUrl: './tecnologia-update.component.html',
  styleUrls: ['./tecnologia-update.component.css']
})
export class TecnologiaUpdateComponent implements OnInit {

  tecnologia: Tecnologia;

  constructor(
    private tecnologiaService: TecnologiaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.tecnologiaService.detail(id).subscribe(
      data => {
        this.tecnologia = data;
      },
      err => console.log(err)
    );
  }

  onUpdate(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.tecnologia.id = undefined;
    this.tecnologia.dtRegistro = undefined;
    this.tecnologiaService.update(id, this.tecnologia).subscribe(
      data => {
        this.toastrService.success("Tecnologia alterada com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['/tecnologia/listar']);
  }

}


