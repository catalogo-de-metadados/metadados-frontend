import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TecnologiaUpdateComponent } from './tecnologia-update.component';

describe('TecnologiaUpdateComponent', () => {
  let component: TecnologiaUpdateComponent;
  let fixture: ComponentFixture<TecnologiaUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TecnologiaUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TecnologiaUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
