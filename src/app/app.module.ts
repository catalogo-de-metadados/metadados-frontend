import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from 'ngx-toastr';
import { NgSelectModule } from '@ng-select/ng-select';
import { TabsModule } from "ngx-bootstrap/tabs";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import * as CanvasJSAngularChart from '../assets/canvasjs.angular.component';

import { OAuthModule } from 'angular-oauth2-oidc';

import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { SignupComponent } from './signup/signup.component';
import { BancoCreateComponent } from './banco/create/banco-create.component';
import { BancoUpdateComponent } from './banco/update/banco-update.component';
import { BancoDetailComponent } from './banco/detail/banco-detail.component';
import { BancoListComponent } from './banco/list/banco-list.component';
import { EstatisticaComponent } from './estatistica/estatistica.component';
import { EtiquetaCreateComponent } from './etiqueta/create/etiqueta-create.component';
import { EtiquetaDetailComponent } from './etiqueta/detail/etiqueta-detail.component';
import { EtiquetaUpdateComponent } from './etiqueta/update/etiqueta-update.component';
import { EtiquetaListComponent } from './etiqueta/list/etiqueta-list.component';
import { EsquemaCreateComponent } from './esquema/create/esquema-create.component';
import { EsquemaListComponent } from './esquema/list/esquema-list.component';
import { EsquemaUpdateComponent } from './esquema/update/esquema-update.component';
import { EsquemaDetailComponent } from './esquema/detail/esquema-detail.component';
import { TabelaCreateComponent } from './tabela/create/tabela-create.component';
import { TabelaDetailComponent } from './tabela/detail/tabela-detail.component';
import { TabelaUpdateComponent } from './tabela/update/tabela-update.component';
import { TabelaListComponent } from './tabela/list/tabela-list.component';
import { AtributoCreateComponent } from './atributo/create/atributo-create.component';
import { AtributoDetailComponent } from './atributo/detail/atributo-detail.component';
import { AtributoUpdateComponent } from './atributo/update/atributo-update.component';
import { AtributoListComponent } from './atributo/list/atributo-list.component';
import { LgpdComponent } from './atributo/lgpd/lgpd.component';
import { AmbienteCreateComponent } from './ambiente/create/ambiente-create.component';
import { AmbienteListComponent } from './ambiente/list/ambiente-list.component';
import { AmbienteUpdateComponent } from './ambiente/update/ambiente-update.component';
import { AmbienteDetailComponent } from './ambiente/detail/ambiente-detail.component';
import { TecnologiaCreateComponent } from './tecnologia/create/tecnologia-create.component';
import { TecnologiaListComponent } from './tecnologia/list/tecnologia-list.component';
import { TecnologiaUpdateComponent } from './tecnologia/update/tecnologia-update.component';
import { TecnologiaDetailComponent } from './tecnologia/detail/tecnologia-detail.component';
import { ContribuicaoComponent } from './atributo/contribuicao/contribuicao.component';
import { RelatorioComponent } from './relatorio/relatorio.component';

var CanvasJSChart = CanvasJSAngularChart.CanvasJSChart;

@NgModule({
  declarations: [
    AppComponent,
    CanvasJSChart,
    HomeComponent,
    MenuComponent,
    SignupComponent,
    BancoCreateComponent,
    BancoUpdateComponent,
    BancoDetailComponent,
    BancoListComponent,
    EstatisticaComponent,
    EtiquetaCreateComponent,
    EtiquetaDetailComponent,
    EtiquetaUpdateComponent,
    EtiquetaListComponent,
    EsquemaCreateComponent,
    EsquemaListComponent,
    EsquemaUpdateComponent,
    EsquemaDetailComponent,
    TabelaCreateComponent,
    TabelaDetailComponent,
    TabelaUpdateComponent,
    TabelaListComponent,
    AtributoCreateComponent,
    AtributoDetailComponent,
    AtributoUpdateComponent,
    AtributoListComponent,
    LgpdComponent,
    AmbienteCreateComponent,
    AmbienteListComponent,
    AmbienteUpdateComponent,
    AmbienteDetailComponent,
    TecnologiaCreateComponent,
    TecnologiaListComponent,
    TecnologiaUpdateComponent,
    TecnologiaDetailComponent,
    ContribuicaoComponent,
    RelatorioComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    TooltipModule.forRoot(),
    FormsModule,
    NgSelectModule,
    TabsModule.forRoot(),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [
          // 'http://localhost:8080/banco',
          // 'http://localhost:8080/esquema',
          // 'http://localhost:8080/tabela',
          // 'http://localhost:8080/atributo',
          // 'http://localhost:8080/tipo-etiqueta',
          // 'http://localhost:8080/etiqueta',
          // 'http://localhost:8080/ambiente',
          // 'http://localhost:8080/tecnologia',
          // 'http://localhost:8080/estatistica',
          // 'http://localhost:8080/contribuicao',
          // 'http://localhost:8080/user',
          'https://backend.metadados.org/banco', 
          'https://backend.metadados.org/esquema', 
          'https://backend.metadados.org/tabela', 
          'https://backend.metadados.org/atributo', 
          'https://backend.metadados.org/tipo-etiqueta',
          'https://backend.metadados.org/etiqueta',
          'https://backend.metadados.org/ambiente', 
          'https://backend.metadados.org/tecnologia', 
          'https://backend.metadados.org/estatistica', 
          'https://backend.metadados.org/contribuicao', 
          'https://backend.metadados.org/user', 
        ],
        sendAccessToken: true
      }
    }),
    ToastrModule.forRoot({
      positionClass: 'toast-top-center'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
