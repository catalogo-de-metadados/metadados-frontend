import { Etiqueta } from "./etiqueta";
import { Tabela } from "./tabela";

export class Atributo {
    id: number;
    nmAtributo: string;
    inCampoChave: boolean;
    inCampoFk: boolean;
    inObrigatorio: boolean;
    inPessoal: string;
    inSensivel: string;
    tamanho: number;
    tipo: string;
    txDescricao: string;
    dtRegistro: Date;
    etiqueta: Etiqueta[];
    tabela: Tabela;

    constructor(
        nmAtributo: string,
        inPessoal: string,
        inSensivel: string,
        tipo: string,
        tamanho: number,
        inCampoChave: boolean,
        inCampoFk: boolean,
        inObrigatorio: boolean,
        txDescricao: string,
        etiqueta: Etiqueta[],
        tabela: Tabela
    ) {
        this.nmAtributo = nmAtributo;
        this.inCampoChave = inCampoChave;
        this.inCampoFk = inCampoFk;
        this.inObrigatorio = inObrigatorio;
        this.inPessoal = inPessoal;
        this.inSensivel = inSensivel;
        this.tamanho = tamanho;
        this.tipo = tipo;
        this.txDescricao = txDescricao;
        this.etiqueta = etiqueta;
        this.tabela = tabela;
    }
}





