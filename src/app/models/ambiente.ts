
export class Ambiente {
    id: number;
    nmAmbiente: string;
    dtRegistro: Date;

    constructor(nmAmbiente: string) {
        this.nmAmbiente = nmAmbiente;
    }
}