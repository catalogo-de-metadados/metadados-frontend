
export class Etiqueta {
    id: number;
    nmEtiqueta: string;
    inTipoEtiqueta: string;
    dtRegistro: Date;

    constructor(nmEtiqueta: string, inTipoEtiqueta: string) {
        this.nmEtiqueta = nmEtiqueta;
        this.inTipoEtiqueta = inTipoEtiqueta
    }
}