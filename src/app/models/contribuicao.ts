import { Atributo } from "./atributo";

export class Contribuicao {
    id: number;
    txContribuicao: string;
    usrContribuicao
    atributo: Atributo;
    dtRegistro: Date;

    constructor(txContribuicao: string, ursContribuicao, atributo: Atributo) {
        this.txContribuicao = txContribuicao;
        this.usrContribuicao = ursContribuicao
        this.atributo = atributo;
    }
}
