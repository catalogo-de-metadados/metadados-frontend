import { Esquema } from "./esquema";

export class Tabela {
    id: number;
    nmTabela: string;
    txDescricao: string;
    dtRegistro: Date;
    esquema: Esquema;

    constructor(nmTabela: string, txDescricao: string, esquema: Esquema) {
        this.nmTabela = nmTabela;
        this.txDescricao = txDescricao;
        this.esquema = esquema;
    }
}
