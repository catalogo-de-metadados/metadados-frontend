export class TipoEtiqueta {
    id: number;
    nmTipoEtiqueta: string;
    idControle: string;
    dtRegistro: Date;

    constructor(nmTipoEtiqueta: string, idControle: string) {
        this.nmTipoEtiqueta = nmTipoEtiqueta;
        this.idControle = idControle;
    }
}