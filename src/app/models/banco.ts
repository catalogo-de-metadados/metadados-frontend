import { Ambiente } from "./ambiente";
import { Tecnologia } from "./tecnologia";

export class Banco {
    id: number;
    nmBanco: string;
    ambiente: Ambiente;
    tecnologia: Tecnologia;
    txDescricao: string;
    dtRegistro: Date;

    constructor(nmBanco: string, ambiente: Ambiente, tecnologia: Tecnologia, txDescricao: string) {
        this.nmBanco = nmBanco;
        this.ambiente = ambiente;
        this.tecnologia = tecnologia;
        this.txDescricao = txDescricao;
    }
}
