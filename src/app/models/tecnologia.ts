
export class Tecnologia {
    id: number;
    nmTecnologia: string;
    dtRegistro: Date;

    constructor(nmTecnologia: string) {
        this.nmTecnologia = nmTecnologia;
    }
}