import { Banco } from "./banco";

export class Esquema {
    id: number;
    nmEsquema: string;
    txDescricao: string;
    dtRegistro: Date;
    banco: Banco;

    constructor(nmEsquema: string, txDescricao: string, banco: Banco) {
        this.nmEsquema = nmEsquema;
        this.txDescricao = txDescricao;
        this.banco = banco;
    }
}
