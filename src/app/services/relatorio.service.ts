import { Injectable } from '@angular/core';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { Atributo } from '../models/atributo';
import { Banco } from '../models/banco';
import { AtributoService } from './atributo.service';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class RelatorioService {

  atributos: Atributo[];
  banco: Banco;

  constructor(private atributoService: AtributoService) { }

  private async loadAtributos(banco: Banco) {
    await this.atributoService
      .getAllAtributoPessoalSensivelByBancoId(banco.id)
      .toPromise()
      .then((data) => {
        this.atributos = data;
        this.atributos.map((a) => {
          switch (a.inPessoal) {
            case "S":
              a.inPessoal = "Sim"
              break;
            case "N":
              a.inPessoal = "Não"
              break;
            case "X":
              a.inPessoal = "Não se aplica"
              break;
            default:
              a.inPessoal = "Não Classificado"
              break;
          };
          switch (a.inSensivel) {
            case "S":
              a.inSensivel = "Sim"
              break;
            case "N":
              a.inSensivel = "Não"
              break;
            case "X":
              a.inSensivel = "Não se aplica"
              break;
            default:
              a.inSensivel = "Não Classificado"
              break;
          };
        })
      });
  }

  async generate(banco: Banco) {
    await this.loadAtributos(banco);
    this.banco = banco;
    const documentDefinition = {
      pageOrientation: "landscape",
      footer: function (currentPage, pageCount) {
        return {
          columns: [
            {
              text: `Catálogo de Metadados`,
              fontSize: 8,
              margin: [40, 0, 0, 0],
              width: "*",
            },
            {
              text: `Data: ${new Date().toLocaleDateString()} - Pág ${currentPage.toString()} de ${pageCount}`,
              fontSize: 8,
              alignment: "right",
              margin: [0, 0, 40, 0],
              width: "auto",
            },
          ],
        };
      },
      content: [
        this.tituloRelatorio(),
        this.dadosAtributos()
      ],
      info: {
        title: "Catálogo de Metadados",
        author: "Marcelo Borges",
      },
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 0, 0, 10],
        },
        subheader: {
          fontSize: 12,
          bold: true,
          margin: [0, 10, 0, 5],
        },
        tableExample: {
          fontSize: 10,
          margin: [0, 5, 0, 15],
        },
        tableHeader: {
          bold: true,
          fontSize: 10,
          color: "black",
        },
      },
    };
    pdfMake.createPdf(documentDefinition).open();
  }

  private tituloRelatorio() {
    return [
      {
        text: "Registro dos dados classificados como pessoais e sensíveis",
        bold: true,
        fontSize: 14,
        margin: [0, 0, 0, 12],
      },
      {
        text: "Este documento visa identificar dentro da estrutura de um determinado banco, onde encontram-se os dados classificados de acordo com a LGPD como dados pessoais e/ou sensíveis.",
        fontSize: 10,
        margin: [0, 0, 0, 12],
      },
    ];
  }

  private dadosAtributos() {
    if (this.atributos.length)
      return [
        {
          text: `Banco: ${this.banco.nmBanco}`,
          bold: true,
          fontSize: 12,
        },
        {
          text: `Ambiente: ${this.banco.ambiente.nmAmbiente} 
           Tecnologia: ${this.banco.tecnologia.nmTecnologia}`,
          fontSize: 10,
        },

        {
          style: "tableExample",
          table: {
            headerRows: 1,
            widths: ["*", "*", "*", 80, 80, 200],
            body: [
              [
                { text: "Esquema", style: "tableHeader" },
                { text: "Tabela", style: "tableHeader" },
                { text: "Atributos", style: "tableHeader" },
                { text: "Pessoal", style: "tableHeader" },
                { text: "Sensível", style: "tableHeader" },
                { text: "Etiquetas", style: "tableHeader" },
              ],
              ...this.atributos.map((a) => [
                a.tabela.esquema.nmEsquema,
                a.tabela.nmTabela,
                a.nmAtributo,
                a.inPessoal,
                a.inSensivel,
                // `${a.inPessoal = 'S' ? 'Sim' : 'Não'}`,
                // `${a.inSensivel = 'S' ? 'Sim' : 'Não'}`,
                `${a.etiqueta ? a.etiqueta
                  .map((e) => e.nmEtiqueta)
                  .join(";\n") : 'Não há etiquetas'}`,
              ]),
            ],
          },
          layout: {
            hLineColor: function (i, node) {
              return i === 0 || i === node.table.body.length ? "black" : "gray";
            },
            vLineColor: function (i, node) {
              return i === 0 || i === node.table.widths.length
                ? "black"
                : "gray";
            },
          },
        },
      ];
    else
      return [
        {
          text: `Não há dados classificados como pessoais e/ou sensíveis no banco ${this.banco.nmBanco}.`,
          fontSize: 10,
          margin: [0, 0, 0, 12],
        },
      ];
  }






}
