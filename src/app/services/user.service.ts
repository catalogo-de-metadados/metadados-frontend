import { User } from './../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { UtilService } from 'src/app/services/util.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  URL = `${this.utilService.getUrlBackend()}/user/`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public create(user: User): Observable<any> {
    return this.httpClient.post<any>(this.URL + 'create', user, this.httpOptions);
  }
}
