import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Banco } from '../models/banco'
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class BancoService {

  URL = `${this.utilService.getUrlBackend()}/banco`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public list(): Observable<Banco[]> {
    return this.httpClient.get<Banco[]>(this.URL, this.httpOptions);
  }

  public detail(id: number): Observable<Banco> {
    return this.httpClient.get<Banco>(this.URL + `/${id}`, this.httpOptions);
  }

  public create(banco: Banco): Observable<any> {
    return this.httpClient.post<any>(this.URL, banco, this.httpOptions);
  }

  public update(id: number, banco: Banco): Observable<any> {
    return this.httpClient.put<any>(this.URL + `/${id}`, banco, this.httpOptions);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.URL + `/${id}`, this.httpOptions);
  }
}
