import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contribuicao } from '../models/contribuicao'
import { UtilService } from './util.service';
@Injectable({
  providedIn: 'root'
})
export class ContribuicaoService {

  URL = `${this.utilService.getUrlBackend()}/contribuicao`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public create(contribuicao: Contribuicao): Observable<any> {
    return this.httpClient.post<any>(this.URL, contribuicao, this.httpOptions);
  }

  public listAllContribuicoesByAtributoId(id): Observable<Contribuicao[]> {
    return this.httpClient.get<Contribuicao[]>(`${this.URL}/getAllContribuicoesByAtributoId/${id}`, this.httpOptions);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.URL + `/${id}`, this.httpOptions);
  }


}
