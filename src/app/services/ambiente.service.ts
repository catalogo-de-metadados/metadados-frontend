import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ambiente } from '../models/ambiente'
import { UtilService } from './util.service';
@Injectable({
  providedIn: 'root'
})
export class AmbienteService {
  URL = `${this.utilService.getUrlBackend()}/ambiente`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public list(): Observable<Ambiente[]> {
    return this.httpClient.get<Ambiente[]>(this.URL, this.httpOptions);
  }

  public detail(id: number): Observable<Ambiente> {
    return this.httpClient.get<Ambiente>(this.URL + `/${id}`, this.httpOptions);
  }

  public create(ambiente: Ambiente): Observable<any> {
    return this.httpClient.post<any>(this.URL, ambiente, this.httpOptions);
  }

  public update(id: number, ambiente: Ambiente): Observable<any> {
    return this.httpClient.put<any>(this.URL + `/${id}`, ambiente, this.httpOptions);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.URL + `/${id}`, this.httpOptions);
  }
}
