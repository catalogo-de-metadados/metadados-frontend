import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tecnologia } from '../models/tecnologia'
import { UtilService } from './util.service';
@Injectable({
  providedIn: 'root'
})
export class TecnologiaService {
  URL = `${this.utilService.getUrlBackend()}/tecnologia`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public list(): Observable<Tecnologia[]> {
    return this.httpClient.get<Tecnologia[]>(this.URL, this.httpOptions);
  }

  public detail(id: number): Observable<Tecnologia> {
    return this.httpClient.get<Tecnologia>(this.URL + `/${id}`, this.httpOptions);
  }

  public create(tecnologia: Tecnologia): Observable<any> {
    return this.httpClient.post<any>(this.URL, tecnologia, this.httpOptions);
  }

  public update(id: number, tecnologia: Tecnologia): Observable<any> {
    return this.httpClient.put<any>(this.URL + `/${id}`, tecnologia, this.httpOptions);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.URL + `/${id}`, this.httpOptions);
  }
}
