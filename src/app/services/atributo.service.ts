import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Atributo } from '../models/atributo'
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class AtributoService {

  URL = `${this.utilService.getUrlBackend()}/atributo`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public list(): Observable<Atributo[]> {
    return this.httpClient.get<Atributo[]>(this.URL, this.httpOptions);
  }

  public listAllByTabelaId(id): Observable<Atributo[]> {
    return this.httpClient.get<Atributo[]>(`${this.URL}/getAllAtributosByTabelaId/${id}`, this.httpOptions);
  }

  public getAllAtributoPessoalSensivelByBancoId(id): Observable<Atributo[]> {
    return this.httpClient.get<Atributo[]>(`${this.URL}/getAllAtributoPessoalSensivelByBancoId/${id}`, this.httpOptions);
  }

  public detail(id: number): Observable<Atributo> {
    return this.httpClient.get<Atributo>(this.URL + `/${id}`, this.httpOptions);
  }

  public create(atributo: Atributo): Observable<any> {
    return this.httpClient.post<any>(this.URL, atributo, this.httpOptions);
  }

  public update(id: number, atributo: Atributo): Observable<any> {
    return this.httpClient.put<any>(this.URL + `/${id}`, atributo, this.httpOptions);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.URL + `/${id}`, this.httpOptions);
  }
}