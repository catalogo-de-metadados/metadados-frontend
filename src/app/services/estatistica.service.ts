import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Estatistica } from '../models/estatistica'
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class EstatisticaService {

  URL = `${this.utilService.getUrlBackend()}/estatistica`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public obterQuantidades(): Observable<Estatistica[]> {
    return this.httpClient.get<Estatistica[]>(`${this.URL}/obterQuantidades`, this.httpOptions);
  }

  public obterClassificados(): Observable<Estatistica[]> {
    return this.httpClient.get<Estatistica[]>(`${this.URL}/obterClassificados`, this.httpOptions);
  }
}
