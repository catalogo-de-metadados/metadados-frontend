import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TipoEtiqueta } from '../models/tipo-etiqueta'
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class TipoEtiquetaService {

  URL = `${this.utilService.getUrlBackend()}/tipo-etiqueta`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public list(): Observable<TipoEtiqueta[]> {
    return this.httpClient.get<TipoEtiqueta[]>(this.URL, this.httpOptions);
  }

  public detail(id: number): Observable<TipoEtiqueta> {
    return this.httpClient.get<TipoEtiqueta>(this.URL + `/${id}`, this.httpOptions);
  }

  public create(tipoEtiqueta: TipoEtiqueta): Observable<any> {
    return this.httpClient.post<any>(this.URL, tipoEtiqueta, this.httpOptions);
  }

  public update(id: number, tipoEtiqueta: TipoEtiqueta): Observable<any> {
    return this.httpClient.put<any>(this.URL + `/${id}`, tipoEtiqueta, this.httpOptions);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.URL + `/${id}`, this.httpOptions);
  }
}
