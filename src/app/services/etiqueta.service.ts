import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Etiqueta } from '../models/etiqueta'
import { UtilService } from './util.service';
@Injectable({
  providedIn: 'root'
})
export class EtiquetaService {

  URL = `${this.utilService.getUrlBackend()}/etiqueta`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public list(): Observable<Etiqueta[]> {
    return this.httpClient.get<Etiqueta[]>(this.URL, this.httpOptions);
  }

  public detail(id: number): Observable<Etiqueta> {
    return this.httpClient.get<Etiqueta>(this.URL + `/${id}`, this.httpOptions);
  }

  public create(etiqueta: Etiqueta): Observable<any> {
    return this.httpClient.post<any>(this.URL, etiqueta, this.httpOptions);
  }

  public update(id: number, etiqueta: Etiqueta): Observable<any> {
    return this.httpClient.put<any>(this.URL + `/${id}`, etiqueta, this.httpOptions);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.URL + `/${id}`, this.httpOptions);
  }
}
