import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tabela } from '../models/tabela'
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class TabelaService {

  URL = `${this.utilService.getUrlBackend()}/tabela`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public list(): Observable<Tabela[]> {
    return this.httpClient.get<Tabela[]>(this.URL, this.httpOptions);
  }

  public listAllByEsquemaId(id): Observable<Tabela[]> {
    return this.httpClient.get<Tabela[]>(`${this.URL}/getAllTabelasByEsquemaId/${id}`, this.httpOptions);
  }

  public detail(id: number): Observable<Tabela> {
    return this.httpClient.get<Tabela>(this.URL + `/${id}`, this.httpOptions);
  }

  public create(tabela: Tabela): Observable<any> {
    return this.httpClient.post<any>(this.URL, tabela, this.httpOptions);
  }

  public update(id: number, tabela: Tabela): Observable<any> {
    return this.httpClient.put<any>(this.URL + `/${id}`, tabela, this.httpOptions);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.URL + `/${id}`, this.httpOptions);
  }
}
