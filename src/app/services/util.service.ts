import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  public getUrlBackend(): string {
    // return 'http://localhost:8080';
    return 'https://backend.metadados.org';
  }
}
