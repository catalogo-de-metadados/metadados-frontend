import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Esquema } from '../models/esquema'
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class EsquemaService {

  URL = `${this.utilService.getUrlBackend()}/esquema`;

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private httpClient: HttpClient, private utilService: UtilService) { }

  public list(): Observable<Esquema[]> {
    return this.httpClient.get<Esquema[]>(this.URL, this.httpOptions);
  }

  public listAllByBancoId(id): Observable<Esquema[]> {
    return this.httpClient.get<Esquema[]>(`${this.URL}/getAllEsquemasByBancoId/${id}`, this.httpOptions);
  }

  public detail(id: number): Observable<Esquema> {
    return this.httpClient.get<Esquema>(this.URL + `/${id}`, this.httpOptions);
  }

  public create(esquema: Esquema): Observable<any> {
    return this.httpClient.post<any>(this.URL, esquema, this.httpOptions);
  }

  public update(id: number, esquema: Esquema): Observable<any> {
    return this.httpClient.put<any>(this.URL + `/${id}`, esquema, this.httpOptions);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.URL + `/${id}`, this.httpOptions);
  }
}
