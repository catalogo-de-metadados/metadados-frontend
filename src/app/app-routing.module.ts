import { SignupGuard } from './guards/signup.guard';
import { MetadadosGuard } from './guards/metadados.guard';
import { SignupComponent } from './signup/signup.component';
import { BancoCreateComponent } from './banco/create/banco-create.component';
import { BancoUpdateComponent } from './banco/update/banco-update.component';
import { BancoDetailComponent } from './banco/detail/banco-detail.component';
import { BancoListComponent } from './banco/list/banco-list.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EstatisticaComponent } from './estatistica/estatistica.component';
import { EtiquetaCreateComponent } from './etiqueta/create/etiqueta-create.component';
import { EtiquetaUpdateComponent } from './etiqueta/update/etiqueta-update.component';
import { EtiquetaDetailComponent } from './etiqueta/detail/etiqueta-detail.component';
import { EtiquetaListComponent } from './etiqueta/list/etiqueta-list.component';
import { EsquemaListComponent } from './esquema/list/esquema-list.component';
import { EsquemaDetailComponent } from './esquema/detail/esquema-detail.component';
import { EsquemaUpdateComponent } from './esquema/update/esquema-update.component';
import { EsquemaCreateComponent } from './esquema/create/esquema-create.component';
import { TabelaCreateComponent } from './tabela/create/tabela-create.component';
import { TabelaUpdateComponent } from './tabela/update/tabela-update.component';
import { TabelaDetailComponent } from './tabela/detail/tabela-detail.component';
import { TabelaListComponent } from './tabela/list/tabela-list.component';
import { AtributoCreateComponent } from './atributo/create/atributo-create.component';
import { AtributoUpdateComponent } from './atributo/update/atributo-update.component';
import { AtributoDetailComponent } from './atributo/detail/atributo-detail.component';
import { AtributoListComponent } from './atributo/list/atributo-list.component';
import { LgpdComponent } from './atributo/lgpd/lgpd.component';
import { TecnologiaCreateComponent } from './tecnologia/create/tecnologia-create.component';
import { TecnologiaUpdateComponent } from './tecnologia/update/tecnologia-update.component';
import { TecnologiaDetailComponent } from './tecnologia/detail/tecnologia-detail.component';
import { TecnologiaListComponent } from './tecnologia/list/tecnologia-list.component';
import { AmbienteListComponent } from './ambiente/list/ambiente-list.component';
import { AmbienteDetailComponent } from './ambiente/detail/ambiente-detail.component';
import { AmbienteUpdateComponent } from './ambiente/update/ambiente-update.component';
import { AmbienteCreateComponent } from './ambiente/create/ambiente-create.component';
import { ContribuicaoComponent } from './atributo/contribuicao/contribuicao.component';
import { RelatorioComponent } from './relatorio/relatorio.component';

const routes: Routes = [

  { path: '', component: HomeComponent },

  {
    path: 'banco', canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] },
    children: [
      { path: '', redirectTo: 'listar', pathMatch: 'full' },
      { path: 'listar', component: BancoListComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },
      { path: ':idBanco/detalhar', component: BancoDetailComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },
      { path: ':idBanco/alterar', component: BancoUpdateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      { path: 'incluir', component: BancoCreateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      {
        path: ":idBanco/esquema", canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] },
        children: [
          { path: '', redirectTo: 'listar', pathMatch: 'full' },
          { path: "listar", component: EsquemaListComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },
          { path: ':idEsquema/detalhar', component: EsquemaDetailComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },
          { path: ':idEsquema/alterar', component: EsquemaUpdateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
          { path: 'incluir', component: EsquemaCreateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
          {
            path: ":idEsquema/tabela", canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] },
            children: [
              { path: '', redirectTo: 'listar', pathMatch: 'full' },
              { path: "listar", component: TabelaListComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },
              { path: ':idTabela/detalhar', component: TabelaDetailComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },
              { path: ':idTabela/alterar', component: TabelaUpdateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
              { path: 'incluir', component: TabelaCreateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
              {
                path: ":idTabela/atributo", canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] },
                children: [
                  { path: '', redirectTo: 'listar', pathMatch: 'full' },
                  { path: "listar", component: AtributoListComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },
                  { path: ':idAtributo/detalhar', component: AtributoDetailComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },
                  { path: ':idAtributo/alterar', component: AtributoUpdateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
                  { path: ':idAtributo/lgpd', component: LgpdComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
                  { path: ':idAtributo/contribuicao', component: ContribuicaoComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },
                  { path: 'incluir', component: AtributoCreateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
                ]
              },
            ]
          },
        ]
      },
    ]
  },

  {
    path: 'ambiente', canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] },
    children: [
      { path: '', redirectTo: 'listar', pathMatch: 'full' },
      { path: 'listar', component: AmbienteListComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      { path: ':id/detalhar', component: AmbienteDetailComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      { path: ':id/alterar', component: AmbienteUpdateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      { path: 'incluir', component: AmbienteCreateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
    ]
  },

  {
    path: 'tecnologia', canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] },
    children: [
      { path: '', redirectTo: 'listar', pathMatch: 'full' },
      { path: 'listar', component: TecnologiaListComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      { path: ':id/detalhar', component: TecnologiaDetailComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      { path: ':id/alterar', component: TecnologiaUpdateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      { path: 'incluir', component: TecnologiaCreateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
    ]
  },
  {
    path: 'etiqueta', canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] },
    children: [
      { path: '', redirectTo: 'listar', pathMatch: 'full' },
      { path: 'listar', component: EtiquetaListComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      { path: ':id/detalhar', component: EtiquetaDetailComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      { path: ':id/alterar', component: EtiquetaUpdateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
      { path: 'incluir', component: EtiquetaCreateComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin'] } },
    ]
  },

  { path: 'estatistica', component: EstatisticaComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },
  { path: 'relatorio', component: RelatorioComponent, canActivate: [MetadadosGuard], data: { requiredRoles: ['admin', 'user'] } },

  { path: 'signup', component: SignupComponent, canActivate: [SignupGuard] },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
