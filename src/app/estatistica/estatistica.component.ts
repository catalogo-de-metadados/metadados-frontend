import { Component, DoCheck, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { EstatisticaService } from 'src/app/services/estatistica.service';
import * as CanvasJS from 'src/assets/canvasjs.min';


@Component({
  selector: 'app-estatistica',
  templateUrl: './estatistica.component.html',
  styleUrls: ['./estatistica.component.css']
})
export class EstatisticaComponent implements OnInit {

  constructor(private estatisticaService: EstatisticaService) { }

  ngOnInit(): void {
    this.graficoQuantidades();
    this.graficoClassificados();
  }

  graficoQuantidades() {
    this.estatisticaService.obterQuantidades().subscribe(
      dados => {
        let chart = new CanvasJS.Chart("chartContainerQuantidades", {
          theme: "light1", // "light2", "dark1", "dark2"
          title: {
            text: "Quantidade de registros"
          },
          data: [
            {
              type: "column", // Change type to "bar", "area", "spline", "pie",etc.
              dataPoints: dados
            }
          ]
        });
        chart.render();
      },
      err => console.log(err)
    );
  }

  graficoClassificados() {
    this.estatisticaService.obterClassificados().subscribe(
      dados => {
        let chart = new CanvasJS.Chart("chartContainerClassificados", {
          theme: "light1", // "light2", "dark1", "dark2"
          title: {
            text: "Classificações LGPD"
          },
          data: [
            {
              type: "pie", // Change type to "bar", "area", "spline", "pie",etc.
              dataPoints: dados
            }
          ]
        });
        chart.render();
      },
      err => console.log(err)
    );

  }

}
