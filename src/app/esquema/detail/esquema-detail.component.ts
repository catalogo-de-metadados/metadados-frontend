import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Esquema } from 'src/app/models/esquema';
import { EsquemaService } from 'src/app/services/esquema.service';

@Component({
  selector: 'app-esquema-detail',
  templateUrl: './esquema-detail.component.html',
  styleUrls: ['./esquema-detail.component.css']
})
export class EsquemaDetailComponent implements OnInit {

  esquema: Esquema;
  params: any;

  constructor(
    private esquemaService: EsquemaService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
    });
    this.esquemaService.detail(this.params.idEsquema).subscribe(
      data => this.esquema = data,
      err => console.log(err)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema']);
  }

}