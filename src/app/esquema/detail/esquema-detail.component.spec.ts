import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EsquemaDetailComponent } from './esquema-detail.component';

describe('EsquemaDetailComponent', () => {
  let component: EsquemaDetailComponent;
  let fixture: ComponentFixture<EsquemaDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EsquemaDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EsquemaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
