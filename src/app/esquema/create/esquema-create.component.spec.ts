import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EsquemaCreateComponent } from './esquema-create.component';

describe('EsquemaCreateComponent', () => {
  let component: EsquemaCreateComponent;
  let fixture: ComponentFixture<EsquemaCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EsquemaCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EsquemaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
