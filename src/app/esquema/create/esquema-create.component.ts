import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Banco } from 'src/app/models/banco';
import { BancoService } from 'src/app/services/banco.service';
import { Esquema } from './../../models/esquema';
import { EsquemaService } from './../../services/esquema.service';

@Component({
  selector: 'app-esquema-create',
  templateUrl: './esquema-create.component.html',
  styleUrls: ['./esquema-create.component.css']
})
export class EsquemaCreateComponent implements OnInit {

  esquema: Esquema;
  params: any;
  
  nmEsquema: string;
  txDescricao: string;
  banco: Banco;

  constructor(
    private esquemaService: EsquemaService,
    private bancoService: BancoService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
      this.loadBanco(params.idBanco);
    });
  }

  loadBanco(id): void {
    this.bancoService.detail(id).subscribe(
      data => this.banco = data,
      err => console.log(err)
    );
  }

  onCreate(): void {
    this.esquema = new Esquema(this.nmEsquema, this.txDescricao, this.banco);

    this.esquemaService.create(this.esquema).subscribe(
      data => {
        this.toastrService.success("Esquema incluído com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema']);
  }
}
