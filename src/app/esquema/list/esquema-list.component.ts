import { Component, OnInit } from '@angular/core';
import { EsquemaService } from './../../services/esquema.service';
import { LoginService } from './../../services/login.service';
import { ExportService } from './../../services/export.service';
import { Esquema } from './../../models/esquema';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { Banco } from 'src/app/models/banco';
import { Observable } from 'rxjs';
import { BancoService } from 'src/app/services/banco.service';

@Component({
  selector: 'app-esquema-list',
  templateUrl: './esquema-list.component.html',
  styleUrls: ['./esquema-list.component.css']
})
export class EsquemaListComponent implements OnInit {

  esquemas: Esquema[] = [];
  banco: Banco;
  temp: Esquema[];

  isAdmin: boolean;

  params: any;

  constructor(
    private esquemaService: EsquemaService,
    private loginService: LoginService,
    private exportService: ExportService,
    private toastrService: ToastrService,
    private activatedRoute: ActivatedRoute,
    private bancoService: BancoService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
      this.loadEsquemas(params.idBanco);
      this.loadBanco(params.idBanco);
    });
    this.isAdmin = this.loginService.getIsAdmin();
  }

  loadEsquemas(id): void {
    this.esquemaService.listAllByBancoId(id).subscribe(
      data => {
        this.esquemas = data['content'];
        this.temp = this.esquemas;
      },
      err => console.log(err)
    );
  }

  loadBanco(id): void {
    this.bancoService.detail(id).subscribe(
      data => {
        this.banco = data;
      },
      err => console.log(err)
    );
  }

  onDelete(id): void {
    console.log(id)
    this.esquemaService.delete(id).subscribe(
      data => {
        this.toastrService.success(data.message);
        this.loadEsquemas(this.params.idBanco);
      },
      err =>
        this.toastrService.error(err.error.message)
    );
  }

  exportexcel(): void {
    this.exportService.export(this.esquemas, "esquemas");
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.nmEsquema.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.esquemas = temp;
  }

}
