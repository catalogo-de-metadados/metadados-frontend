import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EsquemaListComponent } from './esquema-list.component';

describe('EsquemaListComponent', () => {
  let component: EsquemaListComponent;
  let fixture: ComponentFixture<EsquemaListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EsquemaListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EsquemaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
