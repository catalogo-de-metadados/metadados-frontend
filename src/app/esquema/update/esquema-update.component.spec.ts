import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EsquemaUpdateComponent } from './esquema-update.component';

describe('EsquemaUpdateComponent', () => {
  let component: EsquemaUpdateComponent;
  let fixture: ComponentFixture<EsquemaUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EsquemaUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EsquemaUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
