import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EsquemaService } from 'src/app/services/esquema.service';
import { Esquema } from 'src/app/models/esquema';

@Component({
  selector: 'app-esquema-update',
  templateUrl: './esquema-update.component.html',
  styleUrls: ['./esquema-update.component.css']
})
export class EsquemaUpdateComponent implements OnInit {

  esquema: Esquema;
  params: any;

  constructor(
    private esquemaService: EsquemaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
    });
    this.esquemaService.detail(this.params.idEsquema).subscribe(
      data => this.esquema = data,
      err => console.log(err)
    );
  }

  onUpdate(): void {
    this.esquema.id = undefined;
    this.esquema.dtRegistro = undefined;
    this.esquemaService.update(this.params.idEsquema, this.esquema).subscribe(
      data => {
        this.toastrService.success("Esquema alterado com sucesso.");
        this.voltar();
      },
      err => this.toastrService.error(err.error.message)
    );
  }

  voltar(): void {
    this.router.navigate(['banco', this.params.idBanco, 'esquema']);
  }

}
